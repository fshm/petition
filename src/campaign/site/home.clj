(ns campaign.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [campaign.site :as site]
            [campaign.site.forms :as site.forms]
            [campaign.db :as db]
            [campaign.site.db :as site.db]))

(defn home-page [request]
  (ring-resp/response
   (site/base
    "Protect our right to privacy & free and fair elections"
    [:div.container
     [:div.column
      [:h1.is-size-3.has-text-centered "Sign this petition | இந்த மனுவில் கையெழுத்திடுங்கள் "]
      [:hr]
      [:h1.is-size-1 "நமது தனியுரிமை மற்றும் நியாயமான தேர்தலுக்கான உரிமையை பாதுகாப்பீர் "]

      [:h3.is-size-5 "புதுச்சேரியில் நடைபெற இருக்கின்ற 2021 சட்டமன்ற தேர்தலையொட்டி பாரதிய ஜனதா கட்சி சார்ப்பில் பல்வேறு நபர்களின் கைபேசி எண்ணுக்கு குறுஞ்செய்தி (SMS) அனுப்பப்பட்டுள்ளது. அதில் ஒரு WhatsApp குழுவின் முகவரியும் அடங்கியுள்ளது. அதை திறந்தால் நேரடியாக நாம் வாக்களிக்கப்போகும் வாக்குச்சாவடி பெயரில் ஒரு WhatsApp குழு வருகிறது."
       [:br] [:br]

       [:a  {:href "https://tamil.indianexpress.com/election/puducherry-assembly-election-postponed-chennai-high-court-285973/"} "வழக்கு பற்றிய விவரம்"]

       [:br] [:br]
       "தேர்தல் ஆணையம் வெளியிடும் வாக்காளர் பட்டியலில் வாக்களரின் பெயர், வயது, பாலினம், முகவரி போன்ற தகவல்களே இருக்கும், தனிப்பட்ட தகவல்களான கைபேசி எண்கள் இருக்காது."
       [:br] [:br]

       "பாஜகவோடு துளியும் தொடர்பில்லாதோரின் எண்கள் பாஜகவுக்கு எப்படி கிடைத்தது என்றும் அவர்கள் எந்த வாக்குச்சாவடியைச் சேர்ந்தவர்கள் என்று எப்படி தெரிந்தது என்றும் சென்னை உயர்நீதிமன்றமும், தேர்தல் ஆணையமும் கேள்வி எழுப்பி விசாரணை நடத்தி வருகின்றனர்."
       [:br] [:br]
       "ஏனெனில் இது அப்பட்டமான தனிநபர் தகவல்களின் தனியுரிமை மீறலாகும். நீங்கள் பாஜகவுக்கு மிஸ்டு கால் கொடுக்கவில்லை (அல்லது) பா.ஜ.க.-வை சேர்ந்தவர்கள் உங்களிடம் வந்து உங்கள் கைபேசி எண்ணை பெற்றுச் செல்லவில்லை என்றாலோ, ஆனால் பா.ஜ.க.விடமிருந்து உங்களுக்கு குறுஞ்செய்தி மற்றும் அழைப்புகள் வந்தது/வருகிறது என்றால் கீழ்வரும் படிவத்தை பூர்த்தி செய்யுங்கள்."

       [:br] [:br]

       "உங்களுடைய ஆதரவு இந்த வழக்கிற்கு மேலும் வலுசேர்க்கும். அதே நேரத்தில், இதுபோன்ற விதிமீறல்களின் இனி யாரும் ஈடுபடாமல் இருக்க ஒரு முன் உதாரணமாக இருக்கும்."
       [:br] [:br]]
      [:h1.is-size-1 "Protect our right to privacy and to ensure free and fair elections"]
      [:h3.is-size-5.has-text-justified "As part of campaign for Puducherry Assembly Election 2021, BJP has sent out unsolicited SMS, Calls and WhatsApp messages."
       [:br] [:br]
       "Electoral roll contains Name and Addresses however, BJP has data identifying voter's phone numbers to their addresses, electoral booth and constituency."
       [:br] [:br]
       [:a {:href "https://livelaw.in/news-updates/voters-personal-details-may-have-been-obtained-by-bjp-campaign-purposes-madras-high-court-171808"} "Case details"]
       [:br] [:br]
       "Majority of the people who have received SMS / WhatsApp messages have also linked their phone numbers to Aadhar / UIDAI. This questions the security of data within the Aadhar ecosystem."
       [:br] [:br]
       "This is data theft, thus violation of fundamental right to privacy. Targeted campaigning also influences the free and fair conduct of elections in our nation, which is a breach of the Code of Conduct. BJP has indulged in unfair electoral practices throughout the country, now is the time to put a stop to this."
       [:br] [:br]
       "If you have been invited to BJP WhatsApp groups, received calls, SMS and if you have not shared your details with BJP directly or indirectly, sign the petition to support the case ongoing in Madras High Court."]

      (site.forms/petition-sign-form request)]])))

(defn home-post [request]
  (let [form (:form-params request)
        accept (:accept form)]
    (if (= "on" accept)
      (do
        (site.db/petition-insert db/jdbc (assoc form :accept true))
        (ring-resp/redirect "/success"))
      (ring-resp/redirect "/"))))

(defn success-page [request]
  (ring-resp/response
   (site/base
    "Success"
    [:div.container
     [:div.column
      [:h1.is-size-1 "Successfully submitted"]
      [:h3.is-size-5 "Thank you for participating in the fight for the right to privacy and for free and fair elections"]
      [:br]
      [:h1.is-size-1 "வெற்றிகரமாகச் சேமிக்கப்பட்டது"]
      [:h3.is-size-5 "தனியுரிமைக்கான உரிமை மற்றும் இலவச மற்றும் நியாயமான தேர்தல்களுக்கான போராட்டத்தில் பங்கேற்றதற்கு நன்றி "]
      [:br]
      [:h3.is-size-3 [:a {:href "https://www.facebook.com/sharer/sharer.php?u=https://petition.fshm.in"} "Share on Facebook | முகநூலில் பகிர "]]
      [:br] [:br]
      [:h3.is-size-3 [:a {:href "https://twitter.com/intent/tweet?url=https://petition.fshm.in&text=Protect%20our%20right%20to%20privacy%20and%20to%20ensure%20free%20and%20fair%20elections%0A%0A%E0%AE%A8%E0%AE%AE%E0%AE%A4%E0%AF%81%20%E0%AE%A4%E0%AE%A9%E0%AE%BF%E0%AE%AF%E0%AF%81%E0%AE%B0%E0%AE%BF%E0%AE%AE%E0%AF%88%20%E0%AE%AE%E0%AE%B1%E0%AF%8D%E0%AE%B1%E0%AF%81%E0%AE%AE%E0%AF%8D%20%E0%AE%A8%E0%AE%BF%E0%AE%AF%E0%AE%BE%E0%AE%AF%E0%AE%AE%E0%AE%BE%E0%AE%A9%20%E0%AE%A4%E0%AF%87%E0%AE%B0%E0%AF%8D%E0%AE%A4%E0%AE%B2%E0%AF%81%E0%AE%95%E0%AF%8D%E0%AE%95%E0%AE%BE%E0%AE%A9%20%E0%AE%89%E0%AE%B0%E0%AE%BF%E0%AE%AE%E0%AF%88%E0%AE%AF%E0%AF%88%20%E0%AE%AA%E0%AE%BE%E0%AE%A4%E0%AF%81%E0%AE%95%E0%AE%BE%E0%AE%AA%E0%AF%8D%E0%AE%AA%E0%AF%80%E0%AE%B0%E0%AF%8D"} "Share on Twitter | ட்விட்டரில் பகிர "]]
      [:br] [:br]
      [:h3.is-size-3 [:a {:href "https://wa.me/?text=Sign this petition - https://petition.fshm.in"} "Share on Whatsapp | வாட்ஸ்அப்பில் பகிர"]]]])))

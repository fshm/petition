-- :name constituency-all :? :*
-- :doc Select all constituency
select * from constituency

-- :name petition-insert :! :n
-- :doc Insert a petition
insert into petition
values (:name, :address, :phone, :accept, :whatsapp, :constituency, :other)

-- :name petition-all :? :*
-- :doc Select all constituency
select * from petition

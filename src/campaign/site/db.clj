(ns campaign.site.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "campaign/site/site.sql")

(ns campaign.site.forms
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [campaign.site :as site]
            [campaign.site.db :as site.db]
            [campaign.db :as db]))

(defn constituency-dropdown-all []
  [:div.field [:label.label "Constituency | தொகுதி "]
   [:div.control [:div.select
                  [:select {:name "constituency"}
                   (map (fn [con] [:option {:value (:name con)} (:name con)])
                        (site.db/constituency-all db/jdbc))]]]])

(defn petition-sign-form [request]
  [:form {:method "POST" :action (str "/")}
   (site/form-anti-forgery-token request)

   (site/form-control-input "Name | பெயர் " "name" "Enter your name" :required "required")
   (site/form-control-input "Address | முகவரி " "address" "Enter your address" :required "required")
   (constituency-dropdown-all)
   (site/form-control-input "Phone | தொலைபேசி எண்" "phone" "Enter your phone number" :required "required")
   (site/form-control-input "BJP Whatsapp Group link | வாட்ஸ்அப் குழு இணையமுகவரி " "whatsapp" "Share your BJP Whatsapp group link")
   (site/form-control-input "Other details | பிற தகவல்கள் " "other" "Share any other details")

   [:div.field.has-text-left
    [:label.checkbox      [:input {:type "checkbox" :name "accept" :required "required" :value "on"}] "  I declare that I haven't given my number and other personal details to BJP directly or indirectly to contact me for any campaign related communication." [:br] [:br] " நான் பா.ஜ.க கட்சியின் எந்த ஒரு எண்ணிற்கும் மிஸ்டு கால் கொடுக்கவில்லை என்றும் பா.ஜ.க-வை சார்ந்த எவரும் எங்கள் வீட்டிற்கு வந்து என்னுடைய கைபேசி எண்ணை பெற்றுச் செல்லவில்லை என்றும் நான் உறுதியாக கூறுகிறேன்.
"]]

   [:button.button.is-block.is-info {:type "submit" :id "submit"} "Submit | சமர்ப்பி "]])



(ns campaign.auth.roles.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "campaign/auth/roles/roles.sql")

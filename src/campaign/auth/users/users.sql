-- :name insert-user :! :n
-- :doc Insert a single user returning affected row count
insert into users (email, username, password, created_at)
values (:email, :username, :password, now())

-- :name user-by-username :? :1
-- :doc Get user by username
select * from users
where username = :username

-- :name user-by-email :? :1
-- :doc Get user by email
select * from users
where email = :email

-- :name user-by-email-or-username :? :1
-- :doc Get user by email or username
select * from users
where email = :email or username ilike :username

-- :name purge-by-username :? :1
-- :doc Permanently delete user by username
delete from users
where username = :username

-- :name purge-by-email :? :1
-- :doc Permanently delete user by email
delete from from users
where email = :email

-- :name delete-by-username :! :n
-- :doc Delete user as per username
update users set deleted = true, deleted_at = now()
where username = :username

-- :name delete-by-email :! :n
-- :doc Delete user as per email
update users set deleted = true, deleted_at = now()
where email = :email

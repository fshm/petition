(ns campaign.auth.users.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "campaign/auth/users/users.sql")
